package com.chrunchyjesus.simplecrop.utils;

import android.content.Context;
import android.content.Intent;

public class IntentUtils {
    public static Intent createAnimationlessIntent(Context packageContext, Class<?> cls) {
        Intent intent = new Intent(packageContext, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        return intent;
    }
}
