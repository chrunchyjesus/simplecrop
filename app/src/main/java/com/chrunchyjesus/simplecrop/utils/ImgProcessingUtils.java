package com.chrunchyjesus.simplecrop.utils;

import android.graphics.Bitmap;

/**
 * Created by chrunchyjesus on 21.02.18.
 */

public class ImgProcessingUtils {
    public static int trimBoundaries(int value) {
        if(value > 255)
            value = 255;
        else if(value < 0)
            value = 0;

        return value;
    }

    public static int[] getPixelArrayFromBitmap(Bitmap bitmap) {
        int[] arr = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(arr, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());

        return arr;
    }

    public static int[] pixelToARGBArray(int pixel) {
        int[] argb = new int[4];
        argb[0] = (pixel & 0xFF000000) >> (6 * 4); // a
        argb[1] = (pixel & 0x00FF0000) >> (4 * 4); // r
        argb[2] = (pixel & 0x0000FF00) >> (2 * 4); // g
        argb[3] = (pixel & 0x000000FF);            // b

        return argb;
    }

    public static int ARGBArrayToPixel(int[] argb) {
        return (argb[0] << (6 * 4)) +
               (argb[1] << (4 * 4)) +
               (argb[2] << (2 * 4)) +
               argb[3];
    }
}
