package com.chrunchyjesus.simplecrop;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.widget.SeekBar;

public class ControlBrightnessActivity extends ProcessmodeActivity {
    Bitmap img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_brightness);

        imgview = findViewById(R.id.imageView);
        img = getIntentSourceImg();
        imgview.setImageBitmap(img.copy(Bitmap.Config.ARGB_8888, true));

        setupSeekbar();
    }

    public void setupSeekbar() {
        SeekBar sk = findViewById(R.id.brightness_seekBar);

        sk.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int brightnessValue = seekBar.getProgress();
                Bitmap bitmap = adjustBrightness(brightnessValue);
                imgview.setImageBitmap(bitmap);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {}
        });
    }

    private Bitmap adjustBrightness(int val) {
        float b = val * 0.01f;

        Bitmap img = render(new ColorMatrix(new float[] {
                b,  0,  0,  0, 0,
                0,  b,  0,  0, 0,
                0,  0,  b,  0, 0,
                0,  0,  0,  1, 0
        }));

        return img;
    }

    private Bitmap render(ColorMatrix m) {
        Bitmap tmp = Bitmap.createBitmap(this.img);
        Bitmap bitmap = Bitmap.createBitmap(tmp.getWidth(),
                tmp.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(m));
        canvas.drawBitmap(tmp, 0, 0, paint);

        return bitmap;
    }
}
