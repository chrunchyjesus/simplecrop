package com.chrunchyjesus.simplecrop;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;

public class ControlFilterActivity extends ProcessmodeActivity {
    Bitmap img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_filter);

        img = getIntentSourceImg();

        imgview = findViewById(R.id.imgview);
        imgview.setImageBitmap(img.copy(Bitmap.Config.ARGB_8888, true));
    }

    public void onResetClick(View view) {
        imgview.setImageBitmap(img.copy(Bitmap.Config.ARGB_8888, true));
    }

    public void onBinaryClick(View view) {
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);

        float m = 255f;
        float t = -255*128f;
        ColorMatrix threshold = new ColorMatrix(new float[] {
                m, 0, 0, 1, t,
                0, m, 0, 1, t,
                0, 0, m, 1, t,
                0, 0, 0, 1, 0
        });

        // Convert to grayscale, then scale and clamp
        colorMatrix.postConcat(threshold);

        Bitmap img = render(colorMatrix);

        imgview.setImageBitmap(img);
    }

    public void onGreyscaleClick(View view) {
        Bitmap img = render(new ColorMatrix(new float[] {
                0.213f, 0.715f, 0.072f, 0, 0,
                0.213f, 0.715f, 0.072f, 0, 0,
                0.213f, 0.715f, 0.072f, 0, 0,
                0,      0,      0,      1, 0
        }));

        imgview.setImageBitmap(img);
    }

    public void onSepiaClick(View view) {
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);

        ColorMatrix colorScale = new ColorMatrix();
        colorScale.setScale(1, 1, 0.8f, 1);

        // Convert to grayscale, then apply brown color
        colorMatrix.postConcat(colorScale);

        Bitmap img = render(colorMatrix);

        imgview.setImageBitmap(img);
    }

    public void onInvertClick(View view) {
        Bitmap img = render(new ColorMatrix(new float[] {
                -1,  0,  0,  0, 255,
                0, -1,  0,  0, 255,
                0,  0, -1,  0, 255,
                0,  0,  0,  1,   0
        }));

        imgview.setImageBitmap(img);
    }

    private Bitmap render(ColorMatrix m) {
        Bitmap tmp = Bitmap.createBitmap(this.img);
        Bitmap bitmap = Bitmap.createBitmap(tmp.getWidth(),
                tmp.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(m));
        canvas.drawBitmap(tmp, 0, 0, paint);

        return bitmap;
    }
}
