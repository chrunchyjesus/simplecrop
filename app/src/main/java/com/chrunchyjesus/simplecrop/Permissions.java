package com.chrunchyjesus.simplecrop;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

public class Permissions {
    public static final int REQUEST_WRITE_PERMISSION = 0;
    private static final String writePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE;

    public static boolean hasWritePermission(Activity a) {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(a, writePermission)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        return false;
    }

    public static void requestWritePermission(Activity a) {
        // Permission is not granted
        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(a, writePermission)) {
            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.

            ActivityCompat.requestPermissions(a, new String[]{writePermission}, REQUEST_WRITE_PERMISSION);
        } else {
            // No explanation needed; request the permission
            ActivityCompat.requestPermissions(a, new String[]{writePermission}, REQUEST_WRITE_PERMISSION);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
    }

    public static boolean checkRequestPermissionsSuccess(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != REQUEST_WRITE_PERMISSION) {
            return false;
        }

        for (int i = 0; i < permissions.length; i++) {
            String permission = permissions[i];
            int grantResult = grantResults[i];

            if (!permission.equals(writePermission)) {
                continue;
            }

            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                return true;
            }
        }

        return false;
    }
}
